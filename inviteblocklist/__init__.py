import json
from pathlib import Path

from masterflix.core.bot import MasterFlix

from .inviteblocklist import InviteBlocklist

with open(Path(__file__).parent / "info.json") as fp:
    __masterflix_end_user_data_statement__ = json.load(fp)["end_user_data_statement"]


async def setup(bot: MasterFlix):
    cog = InviteBlocklist(bot)
    await bot.add_cog(cog)