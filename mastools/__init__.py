from masterflix.core.bot import MasterFlix
from .mastools import MasTools

__masterflix_end_user_data_statement__ = (
    "This cog does not persistently store data or metadata about users."
)


async def setup(bot: MasterFlix):
    cog = MasTools(bot)
    await bot.add_cog(cog)