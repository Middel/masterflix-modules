from masterflix.core.bot import MasterFlix

from .counting import Counting

__masterflix_end_user_data_statement__ = "This cog does not persistently store data about users."


async def setup(bot: MasterFlix) -> None:
    cog = Counting(bot)
    await bot.add_cog(cog)